# User Guide

Okay, so you basically have two different modes. You have the Manual mode, and the internet mode. 

## All Modes

The LEDs display the time. The top row is the hour (24-hour format), the middle row is minutes, and the bottom row is seconds. 

The blue LEDs on the far left and far right are only there to make the clock easier to read at a distance; so you can see how wide each row is. 

Each row of LEDs has 6 bits. If the LED is on (red), that means the bit is high, or a 1. If the LED is off, the bit is low, and is a 0. 

To read the clock, go one row at a time. Each LED has a number printed below it, that's the digits decimal value. Starting at 0, add all the numbers in that row that have an LED on, and that's the decimal value. 

## Manual Mode

To make sure your clock is in the manual mode, ensure that DIP switch 1 is in the down position. The switches are on the top left corner of the board, and the top switch has `Mode` printed on the circuit board beneath it. With this switch down, the rest of the switches don't matter: they are only used for the Internet mode. When powered on with this switch down, the green LED with the word `Manual` printed on the circuit board near it should light up. 

In manual mode, operation of the clock is simple. When you turn on the clock it will start at 00:00:00 and start counting up. There are 6 buttons between the Teensy and the Ethernet port. Hour+ adds an hour to the time, Hour- subtracts an hour, etc.

Simply set the time, and hope my code doesn't introduce too much drift into the clock. 

Oh, and since I'm using the [millis()](https://www.arduino.cc/reference/en/language/functions/time/millis/) function in the Arduino library, the clock will need to be reset after about 50 days. I might fix this issue in a later commit. 

## Internet Mode

To make sure your clock is in the internet mode, ensure that DIP switch 1 is in the up position. With this switch up, the rest of the switches are used to set your timezone. When powered on in internet mode, the green LED with the word `Internet` printed on the circuit board near it should light up. Once the Teensy receives an IP address from DHCP and connects to `time.nist.gov`, the next led `Connected` will light up green as well. 

This mode requires an ethernet connection to a network that service IP addresses via DHCP. If your network doesn't do this, then I can show you how to configure a static IP in the code. 