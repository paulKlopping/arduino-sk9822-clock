# A binary clock.

## Dip switches

https://www.mouser.com/ProductDetail/Omron-Electronics/A6FR-7104?qs=sGAEpiMZZMv%2f%252b2JhlA6ysKug8ENwXImzllGCN6cspcA%3d

## Bypass Caps

https://www.mouser.com/ProductDetail/AVX/18123C104MAT2A?qs=sGAEpiMZZMs0AnBnWHyRQKdiqyDPVQdAl9NqFHUy6K4%3d

## Bulk Caps

https://www.mouser.com/ProductDetail/Panasonic/EEE-1AA101WR?qs=sGAEpiMZZMtZ1n0r9vR22cS6GEbW2ftogJPVpyduo%252bI%3d

## Switches

https://www.mouser.com/ProductDetail/ALPS/SKHHAJA010?qs=sGAEpiMZZMsgGjVA3toVBEoan%252bH53eRJw7EZL1wojxE%3d

## LEDs

https://www.mouser.com/ProductDetail/Kingbright/APT3216LZGCK?qs=sGAEpiMZZMseGfSY3csMkYfrPUYGlFxbkavdINSCqfa7eH3AmSrGbQ%3d%3d

## Resistors

https://www.mouser.com/ProductDetail/Bourns/CRM1206-FX-R330ELF?qs=Zq5ylnUbLm4xRaC1BHqgYg%3d%3d	