#include <SPI.h>
#include <UIPEthernet.h>
#include <NTPClient.h>
#include "FastLED.h"

EthernetClient ethClient;
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
EthernetUDP udp_ntp;
NTPClient timeClient(udp_ntp, "time.nist.gov", (-6*3600), 300000);

#define DATA_PIN 8
#define CLK_PIN 7
#define LED_TYPE SK9822
#define COLOR_ORDER BGR
#define NUM_LEDS 24
CRGB leds[NUM_LEDS];
#define BRIGHTNESS 4
#define FRAMES_PER_SECOND 120;

// #define DEBUG 1asdf

#define green_led_brightness 4
#define connected_led 21
#define internet_led 22
#define manual_led 23

#define HOUR_MINUS_PIN 1
bool hour_minus;
#define HOUR_PLUS_PIN 2
bool hour_plus;
#define MINUTE_MINUS_PIN 3
bool minute_minus;
#define MINUTE_PLUS_PIN 4
bool minute_plus;
#define SECOND_MINUS_PIN 5
bool second_minus;
#define SECOND_PLUS_PIN 6
bool second_plus;

#define DIP_MODE_PIN 14
bool dip_mode; // 1 is internet, 0 is manual
#define DIP_SIGN_PIN 15
bool dip_sign;
#define DIP_16_PIN 16
bool dip_16;
#define DIP_8_PIN 17
bool dip_8;
#define DIP_4_PIN 18
bool dip_4;
#define DIP_2_PIN 19
bool dip_2;
#define DIP_1_PIN 20
bool dip_1;

bool is_connected = false;
int oldUTCTimeOffset = 0;
int UTCTimeOffset = 0;
long manualTimeOffset = 0;
unsigned long lastMillis = 0;
unsigned long currentMillis = 0;
int hours, minutes, seconds, _seconds;
int newUTCOffset = 0;
int sign = 0;
int led_index, led_i;
CRGB color;

void setup() {

  pinMode(connected_led, OUTPUT);
  pinMode(internet_led, OUTPUT);
  pinMode(manual_led, OUTPUT);
  
  pinMode(HOUR_MINUS_PIN, INPUT_PULLUP);
  pinMode(HOUR_PLUS_PIN, INPUT_PULLUP);
  pinMode(MINUTE_MINUS_PIN, INPUT_PULLUP);
  pinMode(MINUTE_PLUS_PIN, INPUT_PULLUP);
  pinMode(SECOND_MINUS_PIN, INPUT_PULLUP);
  pinMode(SECOND_PLUS_PIN, INPUT_PULLUP);
  pinMode(DIP_MODE_PIN, INPUT_PULLUP);
  pinMode(DIP_SIGN_PIN, INPUT_PULLUP);
  pinMode(DIP_16_PIN, INPUT_PULLUP);
  pinMode(DIP_8_PIN, INPUT_PULLUP);
  pinMode(DIP_4_PIN, INPUT_PULLUP);
  pinMode(DIP_2_PIN, INPUT_PULLUP);
  pinMode(DIP_1_PIN, INPUT_PULLUP);

 
  FastLED.addLeds<LED_TYPE, DATA_PIN, CLK_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  FastLED.clear();
  FastLED.show();

  // put your setup code here, to run once:
  #ifdef DEBUG
    Serial.begin(9600);
    delay(3000);
    Serial.println("Begin");
  #endif
}

void loop() {
  //Check Switches
  read_switches();

  if (dip_mode) {
    //internet
    if (!is_connected) {
      FastLED.clear();
      FastLED.show();
      connect_ethernet();
    }
  }
  else
  {
    if (is_connected){
      disconnect_ethernet();
      FastLED.clear();
      FastLED.show();
    }
    //manual
  }

  hours = 1;
  minutes = 2;
  seconds = 3;

  if (is_connected) {
    analogWrite(connected_led, green_led_brightness);
    readUTCOffset();
    if (oldUTCTimeOffset != UTCTimeOffset) {
      timeClient.setTimeOffset(UTCTimeOffset);
      oldUTCTimeOffset = UTCTimeOffset;
    }
    timeClient.update();
    
    hours = timeClient.getHours();
    minutes = timeClient.getMinutes();
    seconds = timeClient.getSeconds();
  }
  else 
  {
    readTimeAdjust();

    //This is probably a horrendous way to do this
    // If one second has elapsed, add it to our offset
    currentMillis = millis();
    if (currentMillis - 1000 > lastMillis) {
      manualTimeOffset += 1;
      //Only update current once per second(ish)
      lastMillis += 1000;
    }

    _seconds = manualTimeOffset;
    _seconds = _seconds % 86400;
    // _seconds += manualTimeOffset;

    hours = (_seconds / 3600);
    minutes = (_seconds - (hours * 3600))/ 60;
    seconds = _seconds - (hours * 3600) - (minutes * 60);
    analogWrite(connected_led, 0);
  }

#ifdef DEBUG
  Serial.print("Time: ");
  Serial.print(hours);
  Serial.print(":");
  Serial.print(minutes);
  Serial.print(":");
  Serial.print(seconds);
  Serial.print(" - ");
  Serial.print(UTCTimeOffset, DEC);
  Serial.print(" - ");
  Serial.print(manualTimeOffset, DEC);
  Serial.println();
#endif

  FastLED.clear();
  display_number(hours, 0);
  display_number(minutes, 1);
  display_number(seconds, 2);
  FastLED.show();

  delay(249);
}

void connect_ethernet() {
  if (Ethernet.begin(mac) == 0) {
    #ifdef DEBUG
      Serial.println("Shits broke yo");
    #endif
    is_connected = false;
  }
  else 
  {
    is_connected = true;
  }
  timeClient.begin();
  
}

void disconnect_ethernet() {
  timeClient.end();
  is_connected = false;
}

void read_switches(){
  dip_mode = digitalRead(DIP_MODE_PIN);
  if (dip_mode) {
    //internet
    analogWrite(internet_led, green_led_brightness);
    analogWrite(manual_led, 0);
  } else {
    //manual
    hour_minus = !digitalRead(HOUR_MINUS_PIN);
    hour_plus = !digitalRead(HOUR_PLUS_PIN);
    minute_minus = !digitalRead(MINUTE_MINUS_PIN);
    minute_plus = !digitalRead(MINUTE_PLUS_PIN);
    second_minus = !digitalRead(SECOND_MINUS_PIN);
    second_plus = !digitalRead(SECOND_PLUS_PIN);
    analogWrite(internet_led, 0);
    analogWrite(connected_led, 0);
    analogWrite(manual_led, green_led_brightness);
  }

  dip_sign = digitalRead(DIP_SIGN_PIN);
  dip_16 = digitalRead(DIP_16_PIN);
  dip_8 = digitalRead(DIP_8_PIN);
  dip_4 = digitalRead(DIP_4_PIN);
  dip_2 = digitalRead(DIP_2_PIN);
  dip_1 = digitalRead(DIP_1_PIN);
}

void readUTCOffset() {
  newUTCOffset = 0;
  sign = (dip_sign) ? 1 : -1;
  if (dip_1)
    newUTCOffset = newUTCOffset + (sign * 1);
  if (dip_2)
    newUTCOffset = newUTCOffset + (sign * 2);
  if (dip_4)
    newUTCOffset = newUTCOffset + (sign * 4);
  if (dip_8)
    newUTCOffset = newUTCOffset + (sign * 8);
  if (dip_16)
    newUTCOffset = newUTCOffset + (sign * 16);

  UTCTimeOffset = 3600 * newUTCOffset;
}

void readTimeAdjust() {
    #ifdef DEBUG
      Serial.print("pre-button: ");
      Serial.print(manualTimeOffset);
    #endif

  if (hour_plus)
    manualTimeOffset += 3600;
  if (hour_minus)
    manualTimeOffset -= 3600;
  if (minute_plus)
    manualTimeOffset += 60;
  if (minute_minus)
    manualTimeOffset -= 60;
  if (second_plus)
    manualTimeOffset += 1;
  if (second_minus)
    manualTimeOffset -= 1;

  Serial.println("");

  //Re-zero if necessary (86400 seconds in a day)
  if (manualTimeOffset >= 86400)
  {
    #ifdef DEBUG
      Serial.print("re-zeroing: ");
      Serial.println(manualTimeOffset);
    #endif
    manualTimeOffset = manualTimeOffset % 86400 ;
  }

  if (manualTimeOffset < 0) {
    #ifdef DEBUG
      Serial.print("Circling around");
      Serial.print(manualTimeOffset);
    #endif
    manualTimeOffset = 86400 + manualTimeOffset;
    #ifdef DEBUG
      Serial.print(" > ");
      Serial.println(manualTimeOffset);
    #endif
  }
}

void display_number(int num, int board) {
  led_index = (8*board);
  for (int i = 0; i < 8; i++) {
    if (i == 0) {
      color = CRGB::Blue;
    }
    else if (i == 7) {
      color = CRGB::Blue;
    }
    else {
      color = (bitRead(num, i-1) == 1) ? CRGB::Red : CRGB::Black;
    }
    led_i = (7+led_index) - i;
    leds[led_i] = color;
  }
}
