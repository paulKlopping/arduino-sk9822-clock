## You can find the User Guide [Here](UserGuide.md)

## Circuit Board Development Setup

Download KiCad

Open the project

Destroy the nets and footprints, and send it to OSHPark for fabrication!

## Teensy Development Setup

1) Download and install the [Arduino IDE](https://www.arduino.cc/en/Main/Software). Do not use the "Windows App" version as it hides the installation directory. Instead, download the Windows Installer.

2) Downlaod and install the [Teensyduino](https://www.pjrc.com/teensy/td_download.html) software.

3) Plug in the Teensy and open the arduino software. 

4) Open the file ArduinoSK9888Clock.ino file. (You did clone the repo, right?)

5) Configure the Arduino software for the Teensy 3.2 (You need to do this every time you open the Arduino IDE)

* Select Teensy 3.2 from the Tools -> Boards menu and wait for it to load. 
* Select 72MHz from the Tools -> CPU Speed menu (no need to overclock)
* Select the port `COMX (Teensy)` where X is a number

6) Install the necessary libraries

* Go to the Sketch Menu -> Include Library -> Manage Libraries

* Find and install 

	* UIPEthernet
	* FastLED
	* NTPClient 
	
7) Click the Upload button (looks like an arrow pointing to the right), and enjoy!